//
//  DetailCountryViewController.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

class DetailCountryViewController: UIViewController {
    let activityIndicator = UIActivityIndicatorView()
    let detailCountryView = DetailCountryView()
    let country: Country
    var infoCountry: DetailCountry?{didSet{
        guard let infoCountry = infoCountry else {
            return
        }
        setInfoCountry(infoCountry)
    }}
    
    init(country: Country) {
        self.country = country
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupDetails()
    }
    
    private func setupView(){
        view.backgroundColor = .white
        view.add(detailCountryView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
                $0.topAnchor.constraint(equalTo: view.topAnchor)
            ])
        }
        view.add(activityIndicator) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ])
            $0.hidesWhenStopped = true
        }
    }
    
    private func setupDetails(){
        if let infoCountry = DetailCountry.get(withCode: country.alpha2Code){
            self.infoCountry = infoCountry
            setupRightBarButtonItem(isSaved: true)
        }else{
            setupRightBarButtonItem(isSaved: false)
            fetchDetails()
        }
    }
    
    private func fetchDetails(){
        activityIndicator.startAnimating()
        Network().load(DetailCountry.fetch(code: country.alpha2Code)) {
            [weak self] result in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                do{
                    let result = try result.get()
                    self?.infoCountry = result
                }catch let error{
                    self?.present(UIAlertController.Error(error: error), animated: true)
                }
            }
        }
    }
    
    private func setInfoCountry(_ info: DetailCountry){
        detailCountryView.sections = SectionInfoCountry.generateSections(info: info)
    }
    
    private func setupRightBarButtonItem(isSaved: Bool){
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: isSaved ? "Delete" : "Save" ,
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(tapRightBarButton))
    }
    
    @objc func tapRightBarButton(){
        if let _ = DetailCountry.get(withCode: country.alpha2Code){
            presentAlert(isSaved: true)
        }else{
            presentAlert(isSaved: false)
        }
    }
    
    private func presentAlert(isSaved: Bool){
        let alertVC = UIAlertController(title: isSaved ? "Delete" : "Save",
                                        message: "Are you sure to \(isSaved ? "delete" : "save") this information?",
            preferredStyle: .actionSheet)
        alertVC.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alertVC.addAction(UIAlertAction(title: "Accept", style: .default, handler: {
            [weak self] _ in
            if isSaved{
                self?.infoCountry?.delete()
                self?.setupRightBarButtonItem(isSaved: false)
            }else{
                self?.infoCountry?.save()
                self?.setupRightBarButtonItem(isSaved: true)
            }
        }))
        present(alertVC, animated: true)
    }
   
}
