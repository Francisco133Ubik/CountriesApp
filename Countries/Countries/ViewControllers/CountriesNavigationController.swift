//
//  CountriesNavigationController.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

class CountriesNavigationController: UINavigationController {
    
    init() {
        super.init(rootViewController: ListCountriesViewController())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        navigationBar.prefersLargeTitles = true
        hidesBottomBarWhenPushed = true
    }


}
