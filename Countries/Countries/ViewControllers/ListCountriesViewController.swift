//
//  ListCountriesViewController.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

class ListCountriesViewController: UIViewController {
    let searchController = UISearchController(searchResultsController: nil)
    let activityIndicator = UIActivityIndicatorView()
    let tableView = UITableView()
    var countriesDatasource = CountriesDataSource()
    var isSearchEmpty: Bool{
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
      return searchController.isActive && !isSearchEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupTableView()
        fetchCountries()
    }
    
    private func setupView(){
        navigationItem.title = "Countries"
        view.backgroundColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Group",
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(addTapped))
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    private func setupTableView(){
        view.add(tableView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
            $0.register(CountryTableViewCell.self, forCellReuseIdentifier: "cell")
            $0.dataSource = countriesDatasource
            $0.delegate = self
        }
        view.add(activityIndicator) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ])
            $0.hidesWhenStopped = true
        }
    }
    
    private func fetchCountries(){
        activityIndicator.startAnimating()
        Network().load(Country.fechAll()) {
            [weak self] result in
            DispatchQueue.main.async {
                self?.activityIndicator.stopAnimating()
                do{
                    let result = try result.get()
                    self?.countriesDatasource.countries = result
                    self?.tableView.reloadData()
                }catch let error{
                    self?.present(UIAlertController.Error(error: error), animated: true)
                }
            }
        }
    }
    
    @objc func addTapped(){
        countriesDatasource.style.toggle()
        if countriesDatasource.style == .group{
            navigationItem.searchController = nil
            navigationItem.rightBarButtonItem?.title = "Ungroup"
        }else{
            navigationItem.searchController = searchController
            navigationItem.rightBarButtonItem?.title = "Group"
        }
        tableView.reloadData()
    }
}

extension ListCountriesViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        countriesDatasource.filterCountriesForSearchText(searchController.searchBar.text)
        countriesDatasource.isFiltering = isFiltering
        tableView.reloadData()
    }
}

extension ListCountriesViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hidesBottomBarWhenPushed = true
        guard let country = countriesDatasource.countrySelected(indexPath: indexPath) else {
            return
        }
        navigationController?.pushViewController(DetailCountryViewController(country: country),
                                                 animated: true)
        hidesBottomBarWhenPushed = false
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
