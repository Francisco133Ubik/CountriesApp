//
//  AreaCountryCollectionViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 06/05/21.
//

import UIKit

class AreaCountryCollectionViewCell: UICollectionViewCell {
    let backgroundContentView = UIView()
    let subregionLabel = UILabel()
    let countryMapImageView = UIImageView()
    let areaLabel = UILabel()
    let latlngLabel = UILabel()
    
    var info: AreaCountry?{didSet{
        guard let info = info else {return}
        subregionLabel.text = info.subregion
        let area = Measurement(value: info.area, unit: UnitArea.squareKilometers)
        let formatter = MeasurementFormatter()
        areaLabel.text = formatter.string(from: area)
        latlngLabel.text = "(\(info.latlng.first?.roundedDouble() ?? 0), \(info.latlng.last?.roundedDouble() ?? 0))"
        countryMapImageView.image = UIImage(named: info.mapCode)
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(backgroundContentView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            $0.backgroundColor = .white
            $0.setRadius()
            $0.dropShadow()
        }
        add(countryMapImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
                $0.topAnchor.constraint(equalTo: topAnchor, constant: 10),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
                $0.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.5)
            ])
            $0.contentMode = .scaleAspectFit
        }
        
        
        add(areaLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: countryMapImageView.trailingAnchor,
                                            constant: 10),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            ])
            $0.textColor = UIColor.gray
        }
        
        add(subregionLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: areaLabel.topAnchor, constant: -20),
                $0.leadingAnchor.constraint(equalTo: countryMapImageView.trailingAnchor,
                                            constant: 10),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            ])
            $0.textColor = UIColor.gray
        }
        
        add(latlngLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: areaLabel.bottomAnchor, constant: 20),
                $0.leadingAnchor.constraint(equalTo: countryMapImageView.trailingAnchor,
                                            constant: 10),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            ])
            $0.textColor = UIColor.gray
        }
    }
}


