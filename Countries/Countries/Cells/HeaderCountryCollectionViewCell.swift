//
//  HeaderCountryCollectionViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 06/05/21.
//

import UIKit

class HeaderCountryCollectionViewCell: UICollectionViewCell {
    let nameLabel = UILabel()
    let flagImageView = UIImageView()
    let nativeNameLabel = UILabel()
    let capitalLabel = UILabel()
    
    var info: HeaderInfoCountry?{didSet{
        guard let info = info else {return}
        nameLabel.text = info.name
        let url = URL(string: URLFlag.appending(info.countryCode.lowercased()).appending(".png"))
        flagImageView.downloaded(from: url)
        nativeNameLabel.text = info.nativeName
        capitalLabel.text = info.capital
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(nameLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.topAnchor.constraint(equalTo: topAnchor)
            ])
            $0.textColor = .black
        }
        add(flagImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 20),
                $0.heightAnchor.constraint(equalToConstant: 100),
                $0.widthAnchor.constraint(equalToConstant: 150),
            ])
            $0.contentMode = .scaleAspectFit
            $0.dropShadow()
        }
        add(nativeNameLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.topAnchor.constraint(equalTo: flagImageView.bottomAnchor, constant: 20),
            ])
            $0.textColor = .black
            $0.font = UIFont.systemFont(ofSize: 17, weight: .black)
        }
        add(capitalLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.topAnchor.constraint(equalTo: nativeNameLabel.bottomAnchor, constant: 10),
            ])
            $0.textColor = UIColor.gray
        }
    }
}
