//
//  BordersCountryCollectionViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 06/05/21.
//

import UIKit

class BordersCountryCollectionViewCell: UICollectionViewCell {
    let backgroundContentView = UIView()
    var collectionView: UICollectionView!
    let titleLabel = UILabel()
    
    var borders = [String](){didSet{
        collectionView.reloadData()
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(backgroundContentView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            $0.backgroundColor = .white
            $0.setRadius()
            $0.dropShadow()
        }
        add(titleLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            ])
            $0.text = "Borders"
            $0.textColor = .gray
        }
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        add(collectionView) {
            $0.backgroundColor = .clear
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
                $0.topAnchor.constraint(equalTo: titleLabel.bottomAnchor),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            $0.isPagingEnabled = true
            $0.register(BorderCollectionViewCell.self, forCellWithReuseIdentifier: "currency")
            $0.delegate = self
            $0.dataSource = self
        }
    }
}


extension BordersCountryCollectionViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "currency", for: indexPath)
        (cell as? BorderCollectionViewCell)?.borderCountry = borders[safe: indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return borders.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3,
                      height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}


class BorderCollectionViewCell: UICollectionViewCell {
    let iconImageView = UIImageView()
    let nameLabel = UILabel()
    
    var borderCountry: String?{didSet{
        guard let borderCountry = borderCountry else {
            return
        }
        iconImageView.image = flag(country: borderCountry).emojiToImage()
        nameLabel.text = Locale.current.localizedString(forRegionCode: borderCountry)
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(iconImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: topAnchor, constant: 10),
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.heightAnchor.constraint(equalToConstant: 40),
                $0.widthAnchor.constraint(equalToConstant: 40)
            ])
            $0.contentMode = .scaleAspectFit
        }
        add(nameLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: iconImageView.bottomAnchor, constant: 10),
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
            ])
            $0.textColor = .gray
        }
    }
    
    func flag(country: String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
}
