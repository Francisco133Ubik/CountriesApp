//
//  CurrencyCountryCollectionViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 06/05/21.
//

import UIKit

class CurrencyCountryCollectionViewCell: UICollectionViewCell {
    let backgroundContentView = UIView()
    var collectionView: UICollectionView!
    
    var currencies = [String](){didSet{
        collectionView.reloadData()
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(backgroundContentView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            $0.backgroundColor = .white
            $0.setRadius()
            $0.dropShadow()
        }
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        add(collectionView) {
            $0.backgroundColor = .clear
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            $0.isPagingEnabled = true
            $0.register(CurrencyCollectionViewCell.self, forCellWithReuseIdentifier: "currency")
            $0.delegate = self
            $0.dataSource = self
        }
    }
}


extension CurrencyCountryCollectionViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "currency", for: indexPath)
        (cell as? CurrencyCollectionViewCell)?.currencyCode = currencies[safe: indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currencies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}


class CurrencyCollectionViewCell: UICollectionViewCell {
    let iconImageView = UIImageView()
    let currencyLabel = UILabel()
    
    var currencyCode: String?{didSet{
        guard let currencyCode = currencyCode else {
            return
        }
        let locale = NSLocale(localeIdentifier: currencyCode)
        currencyLabel.text = locale.localizedString(forCurrencyCode: currencyCode)
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(currencyLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
            ])
            $0.textColor = .gray
        }
        
        add(iconImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.trailingAnchor.constraint(equalTo: currencyLabel.leadingAnchor, constant: -15),
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.heightAnchor.constraint(equalToConstant: 35),
                $0.widthAnchor.constraint(equalToConstant: 35)
            ])
            $0.contentMode = .scaleAspectFit
            $0.image = UIImage(named: "Currency")
        }
    }
}
