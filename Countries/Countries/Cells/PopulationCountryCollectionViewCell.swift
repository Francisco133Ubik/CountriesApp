//
//  PopulationCountryCollectionViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 06/05/21.
//

import UIKit

class PopulationCountryCollectionViewCell: UICollectionViewCell {
    let backgroundContentView = UIView()
    let populationLabel = UILabel()
    let populationImageView = UIImageView()
    let languagesLabel = UILabel()
    let languagesImageView = UIImageView()
    let callingCodesLabel = UILabel()
    let callingCodesImageView = UIImageView()
    
    var info: PopulationCountry?{didSet{
        guard let info = info else {
            return
        }
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        populationLabel.text = numberFormatter.string(from: NSNumber(value: info.population))
        languagesLabel.text = info.languages.map({
            $0
        }).joined(separator: ",")
        callingCodesLabel.text = info.callingCodes.map({
            $0
        }).joined(separator: ",")
        
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(backgroundContentView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            $0.backgroundColor = .white
            $0.setRadius()
            $0.dropShadow()
        }
        add(languagesImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
                $0.heightAnchor.constraint(equalToConstant: 30),
                $0.widthAnchor.constraint(equalToConstant: 30)
            ])
            $0.contentMode = .scaleAspectFit
            $0.image = UIImage(named: "Languages")
        }
        add(languagesLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: languagesImageView.trailingAnchor, constant: 15),
                $0.centerYAnchor.constraint(equalTo: languagesImageView.centerYAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15)
            ])
            $0.textColor = .gray
        }
        add(populationImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: languagesImageView.topAnchor, constant: -10),
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
                $0.heightAnchor.constraint(equalToConstant: 30),
                $0.widthAnchor.constraint(equalToConstant: 30)
            ])
            $0.contentMode = .scaleAspectFit
            $0.image = UIImage(named: "Population")
        }
        add(populationLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: populationImageView.trailingAnchor, constant: 15),
                $0.centerYAnchor.constraint(equalTo: populationImageView.centerYAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15)
            ])
            $0.textColor = .gray
        }
        add(callingCodesImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: languagesImageView.bottomAnchor, constant: 10),
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
                $0.heightAnchor.constraint(equalToConstant: 30),
                $0.widthAnchor.constraint(equalToConstant: 30)
            ])
            $0.contentMode = .scaleAspectFit
            $0.image = UIImage(named: "PhoneCode")
            
        }
        add(callingCodesLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: callingCodesImageView.trailingAnchor, constant: 15),
                $0.centerYAnchor.constraint(equalTo: callingCodesImageView.centerYAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -15)
            ])
            $0.textColor = .gray
        }
    }
    
}
