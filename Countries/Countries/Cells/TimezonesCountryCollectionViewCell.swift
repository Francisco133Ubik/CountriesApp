//
//  TimezonesCountryCollectionViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 06/05/21.
//

import UIKit

class TimezonesCountryCollectionViewCell: UICollectionViewCell {
    let backgroundContentView = UIView()
    let iconImageView = UIImageView()
    let textView = UITextView()
    var timezones = [String](){didSet{
        textView.text = timezones.reduce("\n") {
            $0 + $1.replacingOccurrences(of: "UTC", with: "") + "\n"
        }
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        add(backgroundContentView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 4),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -4),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
            ])
            $0.backgroundColor = .white
            $0.setRadius()
            $0.dropShadow()
        }
        add(iconImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: centerYAnchor),
                $0.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20),
                $0.heightAnchor.constraint(equalToConstant: 35),
                $0.widthAnchor.constraint(equalToConstant: 35)
            ])
            $0.contentMode = .scaleAspectFit
            $0.image = UIImage(named: "Timezones")
        }
        add(textView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -2),
                $0.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 2)
            ])
            $0.textContainerInset = .zero
            $0.textColor = .gray
            $0.textAlignment = .center
            $0.font = UIFont.preferredFont(forTextStyle: .body)
            $0.isEditable = false
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        textView.centerVerticalText()
    }
}


extension UITextView {
    func centerVerticalText() {
        self.textAlignment = .center
        let fitSize = CGSize(width: bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let size = sizeThatFits(fitSize)
        let calculate = (bounds.size.height - size.height * zoomScale) / 2
        let offset = max(1, calculate)
        contentOffset.y = -offset
    }
}
