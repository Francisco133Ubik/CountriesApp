//
//  CountryTableViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    var country: Country?{didSet{
        guard let country = country else {
            return
        }
        textLabel?.text = country.name
        detailTextLabel?.text = [country.alpha2Code, country.alpha3Code].compactMap({$0}).joined(separator: "/")
        imageView?.image = flag(country: country.alpha2Code).emojiToImage()
    }}
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        accessoryType = .disclosureIndicator
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func flag(country: String) -> String {
        let base : UInt32 = 127397
        var s = ""
        for v in country.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
}
