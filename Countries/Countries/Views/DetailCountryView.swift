//
//  DetailCountryView.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import UIKit

class DetailCountryView: UIView {
    var collectionView: UICollectionView!
    var sections = [SectionInfoCountry](){didSet{
        collectionView.reloadData()
    }}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView(){
        backgroundColor = .white
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        add(collectionView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: trailingAnchor),
                $0.topAnchor.constraint(equalTo: topAnchor),
                $0.bottomAnchor.constraint(equalTo: bottomAnchor)
            ])
            $0.backgroundColor = .white
            $0.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "cell")
            $0.register(HeaderCountryCollectionViewCell.self, forCellWithReuseIdentifier: "header")
            $0.register(AreaCountryCollectionViewCell.self, forCellWithReuseIdentifier: "area")
            $0.register(PopulationCountryCollectionViewCell.self, forCellWithReuseIdentifier: "population")
            $0.register(TimezonesCountryCollectionViewCell.self, forCellWithReuseIdentifier: "timezones")
            $0.register(CurrencyCountryCollectionViewCell.self, forCellWithReuseIdentifier: "currencies")
            $0.register(BordersCountryCollectionViewCell.self, forCellWithReuseIdentifier: "borders")
            $0.delegate = self
            $0.dataSource = self
            $0.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 150, right: 0)
            $0.showsVerticalScrollIndicator = false
        }
    }
}

extension DetailCountryView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let section = sections[safe: indexPath.item] else {
            return collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        }
        switch section {
        case .header(let info):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "header", for: indexPath)
            (cell as? HeaderCountryCollectionViewCell)?.info = info
            return cell
        case .area(let info):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "area", for: indexPath)
            (cell as? AreaCountryCollectionViewCell)?.info = info
            return cell
        case .pupulation(let info):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "population", for: indexPath)
            (cell as? PopulationCountryCollectionViewCell)?.info = info
            return cell
        case .timezones(let timezones):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "timezones", for: indexPath)
            (cell as? TimezonesCountryCollectionViewCell)?.timezones = timezones
            return cell
        case .currencies(let currencies):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "currencies", for: indexPath)
            (cell as? CurrencyCountryCollectionViewCell)?.currencies = currencies
            return cell
        case .borders(let borders):
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "borders", for: indexPath)
            (cell as? BordersCountryCollectionViewCell)?.borders = borders
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let section = sections[safe: indexPath.item] else {
            return .zero
        }
        switch section {
        case .header(_):
            return CGSize(width: collectionView.frame.width,
                          height: 210)
        case .area(_):
            return CGSize(width: collectionView.frame.width,
                          height: 140)
        case .pupulation(_):
            return CGSize(width: collectionView.frame.width * 0.56,
                          height: 140)
        case .timezones(_):
            return CGSize(width: collectionView.frame.width * 0.4,
                          height: 140)
        case .currencies(_):
            return CGSize(width: collectionView.frame.width,
                          height: 65)
        case .borders(_):
            return CGSize(width: collectionView.frame.width,
                          height: 120)
        }
    }
}
