//
//  Collection+Extension.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import UIKit

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
