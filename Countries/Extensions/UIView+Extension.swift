//
//  UIView+Extension.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 03/05/21.
//

import UIKit

extension UIView {
    @discardableResult
    func add<T: UIView>(_ subview: T, then closure: (T) -> Void) -> T {
        addSubview(subview)
        closure(subview)
        return subview
    }
    
    func setRadius(_ radius: CGFloat = 8){
        clipsToBounds = true
        layer.masksToBounds = true
        layer.cornerRadius = radius
    }
    
    func dropShadow(shadowOffset: CGSize = CGSize(width: 0, height: 2),
                    radius: CGFloat = 3,
                    opacity: Float = 0.2) {
        layer.borderColor = UIColor.lightGray.cgColor
        layer.masksToBounds = false
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        layer.shadowOffset = shadowOffset
    }
}

