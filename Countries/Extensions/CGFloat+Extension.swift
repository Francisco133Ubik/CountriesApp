//
//  CGFloat+Extension.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

extension CGFloat {
    var dp: CGFloat {
        return (self / 414) * UIScreen.main.bounds.width
    }
}
