//
//  Double+Extension.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import UIKit

extension Double {
    /// Rounds the double to decimal places value
    func roundedDouble(toPlaces places: Int = 1) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
