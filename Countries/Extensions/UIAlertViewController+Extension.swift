//
//  UIAlertViewController+Extension.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

extension UIAlertController {
    static func Error(error: Error) -> UIAlertController {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Accept", style: .default, handler: .none))
        return alert
    }
}
