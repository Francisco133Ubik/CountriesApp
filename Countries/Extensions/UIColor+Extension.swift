//
//  UIColor+Extension.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 03/05/21.
//

import UIKit

extension UIColor{
    static func defaultAppColor() -> UIColor?{
        return UIColor(named: "AccentColor")
    }
}
