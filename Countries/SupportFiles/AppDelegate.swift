//
//  AppDelegate.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 03/05/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var coordinator: MainCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let homeController = UIViewController()
        coordinator = MainCoordinator(mainController: homeController)
        coordinator?.start()
        window.rootViewController = homeController
        window.makeKeyAndVisible()
        window.tintColor = UIColor.defaultAppColor()
        if #available(iOS 13.0, *) {
            window.overrideUserInterfaceStyle = .light
        }
        self.window = window
        return true
    }
}

