
//  LoginViewController.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 03/05/21.
//

import UIKit

class LoginViewController: UIViewController {
    weak var coordinator: MainCoordinator?
    let activityIndicator = UIActivityIndicatorView()
    let loginView = LoginView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        view.backgroundColor = .white
        view.add(loginView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            ])
            $0.delegate = self
        }
        view.add(activityIndicator) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: view.centerYAnchor),
                $0.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            ])
            $0.hidesWhenStopped = true
        }
    }
}


extension LoginViewController: LoginDelegate{
    func signUp(withMail mail: String?, password: String?) {
        do {
            try ValidatorInput.mail.closure(mail)
            try ValidatorInput.password.closure(password)
            activityIndicator.startAnimating()
            Network().mockLoad(MockEndpoint<User>(fileName: "user")) {
                [weak self] result in
                DispatchQueue.main.async {
                    self?.activityIndicator.stopAnimating()
                    do{
                        let user = try result.get()
                        guard user.mail == mail else {
                            self?.present(UIAlertController.Error(error: ValidationError(message: "email incorrect")),
                                          animated: true)
                            return
                        }
                        guard user.password == password else {
                            self?.present(UIAlertController.Error(error: ValidationError(message: "password incorrect")),
                                          animated: true)
                            return
                        }
                        self?.coordinator?.showHome()
                    }catch let error{
                        self?.present(UIAlertController.Error(error: error), animated: true)
                    }
                }
            }
        }catch let error{
            present(UIAlertController.Error(error: error), animated: true)
        }
    }
}
