//
//  LoginView.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 03/05/21.
//

import UIKit

protocol LoginDelegate: class {
    func signUp(withMail mail: String?, password: String?)
}

class LoginView: UIView {
    weak var delegate: LoginDelegate?
    let loginButton = UIButton()
    let mailTextField = InputTextField()
    let passwordTextField = InputTextField()
    let iconImageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        setupNotications()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup(){
        add(loginButton) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -10),
                $0.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 25),
                $0.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -25),
                $0.heightAnchor.constraint(equalToConstant: 60)
            ])
            $0.setTitle("Login", for: .normal)
            $0.setTitleColor(.white, for: .normal)
            $0.backgroundColor = UIColor.defaultAppColor()
            $0.setRadius()
            $0.addTarget(self, action: #selector(tapLogin), for: .touchUpInside)
        }
        
        add(passwordTextField) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: loginButton.topAnchor, constant: -50),
                $0.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 25),
                $0.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -25),
            ])
            $0.textColor = .black
            $0.placeholder = "Password"
            $0.isSecureTextEntry = true
            $0.returnKeyType = .done
            $0.delegate = self
            $0.autocapitalizationType = .none
        }
        
        add(mailTextField) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.bottomAnchor.constraint(equalTo: passwordTextField  .topAnchor, constant: -10),
                $0.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: 25),
                $0.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -25),
            ])
            $0.textColor = .black
            $0.placeholder = "Email"
            $0.keyboardType = .emailAddress
            $0.returnKeyType = .continue
            $0.delegate = self
            $0.autocapitalizationType = .none
        }
        
        add(iconImageView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -100),
                $0.centerXAnchor.constraint(equalTo: centerXAnchor),
                $0.heightAnchor.constraint(equalToConstant: 120),
                $0.widthAnchor.constraint(equalToConstant: 120),
            ])
            $0.image = UIImage(named: "Icon")
        }
    }
    
    private func setupNotications(){
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func tapLogin(){
        delegate?.signUp(withMail: mailTextField.text, password: passwordTextField.text)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        UIView.animate(withDuration: 0.35) {
            self.mailTextField.transform = CGAffineTransform(translationX: 0, y: -180)
            self.passwordTextField.transform = CGAffineTransform(translationX: 0, y: -180)
            self.iconImageView.transform = CGAffineTransform(translationX: 0, y: -80)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.35) {
            self.mailTextField.transform = .identity
            self.passwordTextField.transform = .identity
            self.iconImageView.transform = .identity
        }
    }
    
    
}


extension LoginView: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case mailTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return true
    }
}
