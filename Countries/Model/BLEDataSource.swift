//
//  BLEDataSource.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import UIKit

class BLEDataSource: NSObject, UITableViewDataSource{
    var peripheralDevices = [PeripheralDevice]()
    
    func appendPeripheral(_ peripheral: PeripheralDevice){
        guard let index = peripheralDevices.firstIndex(where: {
            $0.id == peripheral.id
        }) else {
            peripheralDevices.append(peripheral)
            return
        }
        peripheralDevices[index].updateValues(advertisementData: peripheral.advertisementData,
                                              rssi: peripheral.rssi)
    }
    
    func cleanData(){
        peripheralDevices.removeAll()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peripheralDevices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        (cell as? PeripheralDeviceTableViewCell)?.peripheralDevice = peripheralDevices[safe: indexPath.item]
        return cell
    }
}
