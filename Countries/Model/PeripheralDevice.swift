//
//  PeripheralDevice.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import Foundation

struct PeripheralDevice {
    let id: UUID
    let name: String?
    var advertisementData: [String : Any]
    var rssi: NSNumber
    
    mutating func updateValues(advertisementData: [String : Any],
                               rssi: NSNumber){
        self.advertisementData = advertisementData
        self.rssi = rssi
    }
}
