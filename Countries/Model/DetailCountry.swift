//
//  DetailCountry.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import UIKit

struct DetailCountry: Codable {
    let name: String
    let nativeName: String
    let alpha2Code: String
    let alpha3Code: String
    let capital: String
    let currencies: [String]
    let languages: [String]
    let population: Double
    let area: Double?
    let timezones: [String]
    let subregion: String
    let latlng: [Double]
    let callingCodes: [String]
    let borders: [String]
}

extension DetailCountry{
    static func fetch(code: String) -> Endpoint<DetailCountry>{
        let headers = ["x-rapidapi-key": "c220045095msh0e500381f036195p1a352djsnb2b4a91b96e3",
                       "x-rapidapi-host": "restcountries-v1.p.rapidapi.com"]
        return Endpoint(json: .get,
                        url: URL(string: "https://restcountries-v1.p.rapidapi.com/alpha/\(code)")!,
                        headers: headers)
    }
}

extension DetailCountry{
    func save() {
        if let encoded = try? JSONEncoder().encode(self) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: alpha2Code)
        }
    }
    
    func delete(){
        UserDefaults.standard.removeObject(forKey: alpha2Code)
    }
    
    static func get(withCode code: String) -> DetailCountry?{
        if let savedCountry = UserDefaults.standard.object(forKey: code) as? Data {
            return try? JSONDecoder().decode(DetailCountry.self, from: savedCountry)
        }else{
            return nil
        }
    }
}
