//
//  Country.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import Foundation


let URLFlag = "https://flagpedia.net/data/flags/w1160/"

struct Country: Codable {
    let name: String
    let alpha2Code: String
    let alpha3Code: String
    let region: String
}

extension Country{
    static func fechAll() -> Endpoint<[Country]>{
        let headers = ["x-rapidapi-key": "c220045095msh0e500381f036195p1a352djsnb2b4a91b96e3",
                       "x-rapidapi-host": "restcountries-v1.p.rapidapi.com"]
        return Endpoint(json: .get,
                        url: URL(string: "https://restcountries-v1.p.rapidapi.com/all")!,
                        headers: headers)
    }
}
