//
//  SectionInfoCountry.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import UIKit

enum SectionInfoCountry {
    case header(HeaderInfoCountry)
    case area(AreaCountry)
    case borders([String])
    case currencies([String])
    case pupulation(PopulationCountry)
    case timezones([String])
}

extension SectionInfoCountry{
    static func generateSections(info: DetailCountry) -> [SectionInfoCountry]{
        var sections = [SectionInfoCountry]()
        sections.append(.header(HeaderInfoCountry(countryCode: info.alpha2Code,
                                                  name: info.name,
                                                  capital: info.capital,
                                                  nativeName: info.nativeName)))
        if let area = info.area {
            sections.append(.area(AreaCountry(area: area,
                                              mapCode: info.alpha3Code,
                                              subregion: info.subregion,
                                              latlng: info.latlng)))
        }
        sections.append(.pupulation(PopulationCountry(population: info.population,
                                                      languages: info.languages,
                                                      callingCodes: info.callingCodes)))
        if !info.timezones.isEmpty{
            sections.append(.timezones(info.timezones))
        }
        if !info.currencies.isEmpty{
            sections.append(.currencies(info.currencies))
        }
        if !info.borders.isEmpty{
            sections.append(.borders(info.borders))
        }
        return sections
    }
}


struct HeaderInfoCountry {
    let countryCode: String
    let name: String
    let capital: String
    let nativeName: String
}

struct AreaCountry {
    let area: Double
    let mapCode: String
    let subregion: String
    let latlng: [Double]
}

struct PopulationCountry {
    let population: Double
    let languages: [String]
    let callingCodes: [String]
}
