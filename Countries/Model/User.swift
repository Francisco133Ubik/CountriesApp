//
//  User.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import Foundation

struct User: Codable {
    let mail: String
    let password: String
}
