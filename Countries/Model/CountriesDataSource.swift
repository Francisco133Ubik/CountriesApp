//
//  CountriesDataSource.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

enum StyleList {
    case group
    case ungroup
    
    mutating func toggle() {
        switch self {
        case .group: self = .ungroup
        case .ungroup: self = .group
        }
    }
}

class CountriesDataSource: NSObject, UITableViewDataSource {
    var style = StyleList.ungroup
    var sectionTitles = [String]()
    var countriesDictionary = [String: [Country]]()
    var countries = [Country](){didSet{
        setupGroup()
    }}
    var isFiltering = false
    var filterCountries = [Country]()
    
    func setupGroup(){
        for country in countries {
            let regionKey = String(country.region)
            if var countryValues = countriesDictionary[regionKey] {
                countryValues.append(country)
                countriesDictionary[regionKey] = countryValues
            } else {
                countriesDictionary[regionKey] = [country]
            }
        }
        sectionTitles = [String](countriesDictionary.keys)
        sectionTitles = sectionTitles.sorted(by:{ $0 < $1})
    }
    
    func filterCountriesForSearchText(_ text: String?){
        guard let text = text else {
            return
        }
        filterCountries = countries.filter({
            $0.name.lowercased().contains(text.lowercased()) || $0.alpha2Code.lowercased().contains(text.lowercased())
        })
    }
    
    func countrySelected(indexPath: IndexPath) -> Country?{
        switch style {
        case .group:
            let regionKey = sectionTitles[indexPath.section]
            return countriesDictionary[regionKey]?[safe: indexPath.item]
        case .ungroup:
            if isFiltering{
                return filterCountries[safe: indexPath.item]
            }else{
                return countries[safe: indexPath.item]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch style {
        case .group:
            let regionKey = sectionTitles[section]
            if let serviceValue = countriesDictionary[regionKey]{
                return serviceValue.count
            }else{
                return 0
            }
        case .ungroup:
            if isFiltering{
                return filterCountries.count
            }else{
                return countries.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        switch style {
        case .group:
            let regionKey = sectionTitles[indexPath.section]
            (cell as? CountryTableViewCell)?.country = countriesDictionary[regionKey]?[safe: indexPath.item]
        case .ungroup:
            if isFiltering{
                (cell as? CountryTableViewCell)?.country = filterCountries[safe: indexPath.item]
            }else{
                (cell as? CountryTableViewCell)?.country = countries[safe: indexPath.item]
            }
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        switch style {
        case .group: return sectionTitles.count
        case .ungroup: return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch style {
        case .group: return sectionTitles[section]
        case .ungroup: return nil
        }
    }
}
