//
//  ValidatorInput.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 03/05/21.
//

import UIKit

struct ValidatorInput {
    let closure: (String?) throws -> Void
    
    static var mail: ValidatorInput{
        return ValidatorInput { string in
            try validate(!(string ?? "").isEmpty, error: "The email field cannot be empty")
            try validate(isValidEmail(string), error: "Email with wrong format")
        }
    }
    
    static var password: ValidatorInput{
        return ValidatorInput { string in
            try validate(!(string ?? "").isEmpty, error: "The password field cannot be empty")
            try validate((string ?? "").count >= 6 , error: "Password must contain min 6 characters")
        }
    }
    
    static func isValidEmail(_ email: String?) -> Bool {
        guard let email = email else {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
}


struct ValidationError: LocalizedError {
    let message: String
    var errorDescription: String? { return message }
}

func validate(_ condition: @autoclosure () -> Bool,
              error message: @autoclosure () -> String
) throws {
    guard condition() else {
        let message = message()
        throw ValidationError(message: message)
    }
}



