//
//  MainTabBarViewController.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    private func setupView(){
        let countriesController = CountriesNavigationController()
        countriesController.tabBarItem.title = "Countries"
        countriesController.tabBarItem.image = UIImage(named: "CountriesOn")
        
        let bleController = UINavigationController(rootViewController: BLEViewController())
        bleController.navigationBar.prefersLargeTitles = true
        bleController.tabBarItem.title = "BLE"
        bleController.tabBarItem.image = UIImage(named: "BLEOn")
        viewControllers = [countriesController, bleController]
    }
}
