//
//  MainCoordinator.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//


import UIKit

protocol Coordinator {
    var mainController: UIViewController { get set }
    func start()
}

class MainCoordinator: Coordinator {
    var mainController: UIViewController
    
    init(mainController: UIViewController) {
        self.mainController = mainController
    }

    func start() {
        let loginController = LoginViewController()
        loginController.coordinator = self
        mainController.add(loginController)
    }
    
    func showHome(){
        mainController.children.last?.remove()
        mainController.add(MainTabBarViewController())
    }
    
}
