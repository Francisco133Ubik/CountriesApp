//
//  PeripheralDeviceTableViewCell.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 05/05/21.
//

import UIKit

class PeripheralDeviceTableViewCell: UITableViewCell {
    let nameLabel = UILabel()
    let advertisementLabel = UILabel()
    let rssiLabel = UILabel()
    
    
    var peripheralDevice: PeripheralDevice?{didSet{
        guard let peripheralDevice = peripheralDevice else {
            return
        }
        nameLabel.text = peripheralDevice.name ?? peripheralDevice.id.uuidString
        advertisementLabel.text = peripheralDevice.advertisementData.reduce("") {
            $0 + "\($1.key): \($1.value) \n "
        }
        rssiLabel.text = "RSSI: \(peripheralDevice.rssi)"
    }}
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView(){
        selectionStyle = .none
        contentView.add(nameLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
                $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
                $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            ])
            $0.font = UIFont.boldSystemFont(ofSize: 16)
            $0.textColor = .black
        }
        contentView.add(advertisementLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 5),
                $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
                $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
            ])
            $0.numberOfLines = 0
            $0.textColor = UIColor.black.withAlphaComponent(0.8)
        }
        contentView.add(rssiLabel) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.topAnchor.constraint(equalTo: advertisementLabel.bottomAnchor, constant: 5),
                $0.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
                $0.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
                $0.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10)
            ])
            $0.textColor = UIColor.black.withAlphaComponent(0.8)
        }
    }
}
