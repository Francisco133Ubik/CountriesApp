//
//  BLEViewController.swift
//  Countries
//
//  Created by Francisco Javier Delgado García on 04/05/21.
//

import UIKit
import CoreBluetooth

class BLEViewController: UIViewController {
    let tableView = UITableView()
    let bleDataSource = BLEDataSource()
    private var centralManager: CBCentralManager!
    private var myPeripheral: CBPeripheral!
    private var pendingScanWorkItem: DispatchWorkItem?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        centralManager = CBCentralManager(delegate: self, queue: nil)
    }
    
    private func setupView(){
        navigationItem.title = "BLE"
        view.backgroundColor = .white
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Scan",
                                                            style: .done,
                                                            target: self,
                                                            action: #selector(scan))
    }
    
    @objc func scan(){
        bleDataSource.cleanData()
        tableView.reloadData()
        centralManager.scanForPeripherals(withServices: nil, options: nil)
        pendingScanWorkItem?.cancel()
        let stopScanWork = DispatchWorkItem{ [weak self] in
            self?.centralManager.stopScan()
        }
        pendingScanWorkItem = stopScanWork
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(30),
                                      execute: stopScanWork)
    }
    
    private func setupTableView(){
        view.add(tableView) {
            $0.translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                $0.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                $0.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                $0.topAnchor.constraint(equalTo: view.topAnchor),
                $0.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ])
            $0.register(PeripheralDeviceTableViewCell.self, forCellReuseIdentifier: "cell")
            $0.dataSource = bleDataSource
            $0.estimatedRowHeight = 80
            $0.rowHeight = UITableView.automaticDimension
        }
    }
}

extension BLEViewController: CBCentralManagerDelegate, CBPeripheralDelegate{
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        if central.state != CBManagerState.poweredOn {
            present(UIAlertController.Error(error: ValidationError(message: "Unable to use CoreBluetooth")), animated: true)
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        bleDataSource.appendPeripheral(PeripheralDevice(id: peripheral.identifier,
                                                        name: peripheral.name,
                                                        advertisementData: advertisementData,
                                                        rssi: RSSI))
        tableView.reloadData()
    }
}


