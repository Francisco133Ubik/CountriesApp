
# Countries App
iOS technical challenge

![Alt text](https://gitlab.com/Francisco133Ubik/CountriesApp/-/raw/main/Countries/SupportFiles/app.gif)


## Disclaimer

This demo app's purpose is to demonstrate the level of knowledge and usage of Swift and the general understanding of the iOS platform

## License
[MIT](https://choosealicense.com/licenses/mit/)
